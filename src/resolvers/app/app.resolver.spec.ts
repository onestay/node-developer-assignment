import { Test, TestingModule } from '@nestjs/testing';
import { AppResolver } from './app.resolver';
import { AppService } from '../../services/app/app.service';
import { PrismaService } from '../../services/prisma/prisma.service';

describe('AppResolver', () => {
  let appResolver: AppResolver;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [AppService, AppResolver, PrismaService],
    }).compile();

    appResolver = app.get<AppResolver>(AppResolver);
  });

  describe('root', () => {
    it('should be defined', () => {
      expect(appResolver).toBeDefined();
    });
  });
});
