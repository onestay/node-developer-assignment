import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class World {
  @Field()
  text: string;
}
