import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PrismaService } from '../../services/prisma/prisma.service';
import { AppService } from '../../services/app/app.service';
import { User } from './dto/user.dto';
import { World } from './dto/world.dto';

/** A HelloWorld GraphQL resolver. */
@Resolver()
export class AppResolver {
  constructor(
    private readonly appService: AppService,
    private readonly prisma: PrismaService,
  ) {}

  /** An example query. */
  @Query(() => World)
  getHello(): World {
    return {
      text: this.appService.getHello(),
    };
  }

  /** An example mutation. */
  @Mutation(() => User)
  async createUser(@Args('name') name: string): Promise<User> {
    const user = await this.prisma.user.create({
      data: { name },
    });

    return user;
  }
}
