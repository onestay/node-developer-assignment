import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './controllers/app.controller';
import { AppService } from './services/app/app.service';
import { join } from 'path';
import { AppResolver } from './resolvers/app/app.resolver';
import { PrismaService } from './services/prisma/prisma.service';

@Module({
  imports: [
    GraphQLModule.forRootAsync({
      imports: [],
      useFactory: async () => {
        return {
          context: ({ req }) => ({ req }),
          fieldResolverEnhancers: ['interceptors'],
          debug: true,
          playground: true,
          introspection: true,
          autoSchemaFile: join(
            process.cwd(),
            'src',
            'generated',
            './schema.gql',
          ),
          definitions: {
            path: join(process.cwd(), 'src', 'generated', './graphql.ts'),
          },
        };
      },
      inject: [],
    }),
  ],
  controllers: [AppController],
  providers: [AppService, PrismaService, AppResolver],
})
export class AppModule {}
