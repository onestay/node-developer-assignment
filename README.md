A Node.js technical assignment setup using NestJS and Prisma.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Database

Use Prisma to model your database.

```bash
# Launch Prisma Studio
$ yarn prisma:studio

# Generate the typescript types from the Prisma schema.
# You may need to restart your TS server if using VSCode.
$ yarn prisma:schema

# Generate migration files for your updated schema
$ yarn prisma:migrate:create

# Run all migrations on the connected database
$ yarn prisma:migrate:execute
```

## GraphQL

The GraphQL endpoint will run on /graphql on port 3000 in a development
environment. NestJS includes a useful tool to query the endpoint using
http://localhost:3000/graphql, otherwise Postman or similar may be used
when developing.

You can then run the sample query / mutation:

```
query getHello {
  getHello {
    text
  }
}

mutation createUser($name: String!) {
  createUser(name: $name) {
    name
    id
  }
}
```
